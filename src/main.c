#define _DEFAULT_SOURCE

#include "main.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <sys/mman.h>

#define TEST_ERROR_MESSAGE "Test failed"

static void print_freeing() {
    puts("---FREEING---:");
    puts("State:");
}

static void print_newline() {
    puts("\n");
}

static void test_success() {
    void *heap = heap_init(0);
    puts("State before:");
    debug_heap(stdout, heap);

    void *memory = _malloc(128);
    puts("State after:");
    debug_heap(stdout, heap);

    assert(memory != NULL && TEST_ERROR_MESSAGE);

    _free(memory);

    print_freeing();
    debug_heap(stdout, heap);

    heap_term();
}

static void test_one_of_all() {
    void *heap = heap_init(0);
    puts("State before:");
    debug_heap(stdout, heap);

    void *mem1 = _malloc(64);
    void *mem2 = _malloc(128);

    puts("State after:");
    debug_heap(stdout, heap);

    assert(mem1 != NULL && TEST_ERROR_MESSAGE);
    assert(mem2 != NULL && TEST_ERROR_MESSAGE);

    _free(mem1);

    print_freeing();
    debug_heap(stdout, heap);

    _free(mem2);

    heap_term();

}


static void test_two_of_all() {
    void *heap = heap_init(0);
    puts("State before:");
    debug_heap(stdout, heap);

    void *mem1 = _malloc(64);
    void *mem2 = _malloc(128);
    void *mem3 = _malloc(256);
    void *mem4 = _malloc(512);

    puts("State after:");
    debug_heap(stdout, heap);

    assert(mem1 != NULL && TEST_ERROR_MESSAGE);
    assert(mem2 != NULL && TEST_ERROR_MESSAGE);
    assert(mem3 != NULL && TEST_ERROR_MESSAGE);
    assert(mem4 != NULL && TEST_ERROR_MESSAGE);

    _free(mem1);
    _free(mem2);


    print_freeing();
    debug_heap(stdout, heap);

    _free(mem3);
    _free(mem4);

    heap_term();

}

static void test_region_extension() {
    struct region *heap = heap_init(0);
    puts("State before:");
    debug_heap(stdout, heap);


    void *mem1 = _malloc(REGION_MIN_SIZE / 2);
    void *mem2 = _malloc(REGION_MIN_SIZE * 2);

    assert(mem1 != NULL && TEST_ERROR_MESSAGE);
    assert(mem2 != NULL && TEST_ERROR_MESSAGE);
    assert(mem2 - REGION_MIN_SIZE / 2 - offsetof(struct block_header, contents) == mem1 && TEST_ERROR_MESSAGE);

    puts("State after:");
    debug_heap(stdout, heap);

    _free(mem1);
    _free(mem2);

    print_freeing();
    debug_heap(stdout, heap);

    heap_term();

}

static void test_region_extension_trouble() {
    heap_init(0);
    void *mmap_addr = map_pages(HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, MAP_FIXED);

    assert(mmap_addr != MAP_FAILED && TEST_ERROR_MESSAGE);

    puts("State before:");
    debug_heap(stdout, HEAP_START);

    void *mem1 = _malloc(128);
    void *mem2 = _malloc(REGION_MIN_SIZE);
    puts("State after:");
    debug_heap(stdout, HEAP_START);

    void *mem1_header = block_get_header(mem1);
    assert(mem1_header == HEAP_START);

    void *mem2_header = block_get_header(mem2);
    assert(mem2_header != HEAP_START + REGION_MIN_SIZE && TEST_ERROR_MESSAGE);


    _free(mem1);
    _free(mem2);
    print_freeing();
    debug_heap(stdout, HEAP_START);

    heap_term();

}


static void test(testFunction f, char *description) {
    puts(description);
    f();
    print_newline();
}


int main() {
    test(&test_success, "Successful memory allocation");
    test(&test_one_of_all, "Freeing one block from several allocated ones");
    test(&test_region_extension, "The new memory region expands the old one");
    test(&test_region_extension_trouble, "the new memory region does not expand the old one");
    test(&test_two_of_all, "Freeing two blocks from several allocated ones");

}